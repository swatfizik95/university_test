/**
 *  Основной файл роутов
 */

import VueRouter from 'vue-router';

import components from './components';

// список роутов для сайдбара, основные роуты ниже
export let adminLinks = [
    {
        path: '',
        component: components.adminMain,
        name: 'adminMain',
        link_text: 'Главная',
    },
    {
        path: 'groups',
        component: { render: h => h('router-view') },
        link_text: 'Группы',
        children: [
            {
                path: '',
                component: components.groupsList,
                name: 'groupsList',
                link_text: 'Список групп',
            },
            {
                path: 'create',
                component: components.groupsCreate,
                name: 'groupsCreate',
                link_text: 'Добавить группу',
            },
        ],
    },
    {
        path: 'teachers',
        component: { render: h => h('router-view') },
        link_text: 'Учителя',
        children: [
            {
                path: '',
                component: components.teachersList,
                name: 'teachersList',
                link_text: 'Список учителей',
            },
            {
                path: 'create',
                component: components.teachersCreate,
                name: 'teachersCreate',
                link_text: 'Добавить учителя',
            },
        ],
    },
    {
        path: 'subjects',
        component: { render: h => h('router-view') },
        link_text: 'Предметы',
        children: [
            {
                path: '',
                component: components.subjectsList,
                name: 'subjectsList',
                link_text: 'Список предметов',
            },
            {
                path: 'create',
                component: components.subjectsCreate,
                name: 'subjectsCreate',
                link_text: 'Добавить предмет',
            },
        ],
    },
    {
        path: 'students',
        component: { render: h => h('router-view') },
        link_text: 'Студенты',
        children: [
            {
                path: '',
                component: components.studentsList,
                name: 'studentsList',
                link_text: 'Список студентов',
            },
            {
                path: 'create',
                component: components.studentsCreate,
                name: 'studentsCreate',
                link_text: 'Добавить студента',
            },
        ],
    },
    {
        path: 'peka-mode',
        component: components.pekaMode,
        link_text: 'Тест пека мода',
        name: 'pekaMode',
    },
];

// основные роуты
let routes = [
    {
        path: '/vue/admin',
        component: require('../views/master').default,
        children: adminLinks,
    },
];


export default new VueRouter({
    routes,
    mode: 'history',
});