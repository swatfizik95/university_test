/**
 * Этот файл нужен для того, чтобы не засорять импортами
 * файл с роутами
 */
import adminMain from '../views/admin/admin_main';
import groupsList from '../views/admin/groups/list';
import groupsCreate from '../views/admin/groups/create';
import teachersCreate from '../views/admin/teachers/create';
import teachersList from '../views/admin/teachers/list';
import subjectsCreate from '../views/admin/subjects/create';
import subjectsList from '../views/admin/subjects/list';
import studentsCreate from '../views/admin/students/create';
import studentsList from '../views/admin/students/list';
import pekaMode from '../views/admin/test/peka-mode';

let components = {
    adminMain,
    groupsList,
    groupsCreate,
    teachersCreate,
    teachersList,
    subjectsCreate,
    subjectsList,
    studentsCreate,
    studentsList,
    pekaMode
};

export default components;