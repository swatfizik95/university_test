import './bootstrap';
import router from './routes/routes.js';
import Vue from 'vue';

window.appRoot = new Vue({
    el: '#app',
    router,
});
