import axios from 'axios';
import FormErrors from './FormErrors';

export default class Form
{
    /**
     * Получает на вход объект с начальными данными и связывает их с инпутами
     *
     * @param {Object} data
     * @param {String} action
     */
    constructor(data, action)
    {
        this.data = data;
        
        for (let item in data)
        {
            if (data.hasOwnProperty(item))
            {
                this[item] = data[item];
            }
        }
        
        // TODO: сделать класс ошибок
        this.errors = new FormErrors();
        
        this.action = action;
    }
    
    // сброс значений, введёных на форме. при желании, можно сделать второй метод,
    // который будет возвращать данные при инициализации
    reset()
    {
        for (let item in this.data)
        {
            if (this.data.hasOwnProperty(item))
            {
                this[item] = '';
            }
        }
    }
    
    // собираем объект из данных, которые были получены на форме и отправляем их на сервер,
    // после чего очищаем форму
    create()
    {
        let data = {};
        
        for (let item in this.data)
        {
            if (this.data.hasOwnProperty(item))
            {
                data[item] = this[item];
            }
        }
        
        axios.post(this.action, data)
            .then((res) =>
            {
                console.log(res.data);
                this.reset();
            })
            .catch((res) => console.log(res.response));
    }
    
    
}