<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Учитель
    </p>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table>
        <form action="{{ route('teachers_update', [$teacher->id]) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('put') }}
            <tr>
                <td>
                    <image src="{{ $teacher->getImage() }}" style="width: 100px; height: 100px;"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="image" name="image" type="file"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="name" placeholder="Имя" value="{{ old('name', $teacher->name) }}">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="surname" placeholder="Фамилия" value="{{ old('surname', $teacher->surname) }}">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="patronymic" placeholder="Отчество" value="{{ old('patronymic', $teacher->patronymic) }}">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="password" name="password" placeholder="Новый пароль">
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Изменить</button>
                </td>
            </tr>
        </form>
    </table>
    <a href="{{ route('contacts_edit', ['roleUser' => 'teachers', 'id' => $teacher->id]) }}">Контакты</a>
</div>
</body>
</html>