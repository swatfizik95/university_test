<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Учителя
    </p>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table>
        <form action="{{ route('teachers_store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <tr>
                <td>
                    <input type="text" name="name" placeholder="Имя" value="{{ old('name') }}">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="surname" placeholder="Фамилия" value="{{ old('surname') }}">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="patronymic" placeholder="Отчество" value="{{ old('patronymic') }}">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="password" name="password" placeholder="Пароль" value="{{ old('password') }}">
                </td>
            </tr>
            <tr>
                <td>
                    <image id="previewImage" style="display: none; width: 100px; height: 100px;"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="image" name="image" type="file" onload="previewImage()"/>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Отправить</button>
                </td>
            </tr>
        </form>
    </table>
</div>
</body>
</html>