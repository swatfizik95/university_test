<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Учителя
    </p>
    <table>
        <a href="{{ route('teachers_create') }}">Создать</a>
        @if(isset($teachers))
            @foreach ($teachers as $teacher)
                <tr>
                    <td>{{ $teacher->id }}</td>
                    <td>{{ $teacher->name }}</td>
                    <td>{{ $teacher->surname }}</td>
                    <td>{{ $teacher->patronymic }}</td>
                    <td><img src="{{ $teacher->getSmallImage() }}" width="100" height="100"></td>
                    <td>
                        <a href="{{ route('teachers_edit', ['id' => $teacher->id]) }}">Редактировать</a>
                    </td>
                    <td>
                        <form action="{{ route('teachers_destroy', ['id' => $teacher->id]) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <button type="submit">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
</div>
</body>
</html>