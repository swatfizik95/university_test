<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Предмет
    </p>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table>
        <form action="{{ route('subjects_update', [$subject->id]) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('put') }}
            <tr>
                <td>
                    <input type="text" name="name" placeholder="Имя" value="{{ old('name', $subject->name) }}">
                </td>
            </tr>
            <tr>
                <td>
                    <select name="semester_id">
                        @foreach ($semesters as $semester)
                            <option {{ $semester->id == $subject->semester->id ? 'selected' : '' }} value="{{ $semester->id }}">{{ $semester->number }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <select name="group_id">
                        @foreach ($groups as $group)
                            <option {{ $group->id == $subject->group->id ? 'selected' : '' }}  value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <select name="teacher_id">
                        @foreach ($teachers as $teacher)
                            <option {{ $teacher->id == $subject->teacher->id ? 'selected' : '' }} value="{{ $teacher->id }}">{{ $teacher->getFullName() }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Отправить</button>
                </td>
            </tr>
        </form>
    </table>
</div>
</body>
</html>