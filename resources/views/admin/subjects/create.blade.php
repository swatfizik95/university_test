<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Предмет
    </p>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table>
        <form action="{{ route('subjects_store') }}" method="post">
            {{ csrf_field() }}
            <tr>
                <td>
                    <input type="text" name="name" placeholder="Имя" value="{{ old('name') }}">
                </td>
            </tr>
            <tr>
                <td>
                    <select name="semester_id">
                        @foreach ($semesters as $semester)
                            <option value="{{ $semester->id }}">{{ $semester->number }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <select name="group_id">
                        @foreach ($groups as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <select name="teacher_id">
                        @foreach ($teachers as $teacher)
                            <option value="{{ $teacher->id }}">{{ $teacher->getFullName() }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Отправить</button>
                </td>
            </tr>
        </form>
    </table>
</div>
</body>
</html>