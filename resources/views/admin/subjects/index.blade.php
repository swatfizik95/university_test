<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Предметы
    </p>
    <a href="{{ route('subjects_create') }}">Создать</a>
    <table>
        <tr>
            <td>Id</td>
            <td>Название</td>
            <td>Семестр</td>
            <td>Группа</td>
            <td>Учитель</td>
        </tr>
        @if(isset($subjects))

            @foreach ($subjects as $subject)
                <tr>
                    <td>{{ $subject->id }}</td>
                    <td>{{ $subject->name }}</td>
                    <td>{{ $subject->semester->number }}</td>
                    <td>{{ $subject->group->name }}</td>
                    <td>{{ $subject->teacher->getFullName() }}</td>
                    <td>
                        <a href="{{ route('subjects_edit', ['id' => $subject->id]) }}">Редактировать</a>
                    </td>
                    <td>
                        <form action="{{ route('subjects_destroy', ['id' => $subject->id]) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <button type="submit">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
</div>
</body>
</html>