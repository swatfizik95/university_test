<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Группы
    </p>
    <table>
        <a href="{{ route('groups_create') }}">Создать</a>
        @if(isset($groups))
            @foreach ($groups as $group)
                <tr>
                    <td>{{ $group->id }}</td>
                    <td>{{ $group->name }}</td>
                    <td>
                        <a href="{{ route('groups_edit', ['id' => $group->id]) }}">Редактировать</a>
                    </td>
                    <td>
                        <form action="{{ route('groups_destroy', ['id' => $group->id]) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <button type="submit">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
</div>
</body>
</html>