<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Группа
    </p>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table>
        <form action="{{ route('groups_store') }}" method="post">
            {{ csrf_field() }}
            <tr>
                <td>
                    <input type="text" name="name" placeholder="Имя" value="{{ old('name') }}">
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Отправить</button>
                </td>
            </tr>
        </form>
    </table>
</div>
</body>
</html>