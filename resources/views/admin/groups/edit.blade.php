<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Студенты
    </p>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table>
        <form action="{{ route('groups_update', [$group->id]) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('put') }}
            <tr>
                <td>
                    <input type="text" name="name" placeholder="Имя" value="{{ (old('name', $group->name)) }}">
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Изменить</button>
                </td>
            </tr>
        </form>
    </table>
</div>
</body>
</html>