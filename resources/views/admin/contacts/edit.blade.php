<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Контакты
    </p>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table>
        <form action="{{ route('contacts_update', [$roleUser, $id]) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('put') }}
            <input type="hidden" name="user_id" value="{{ $id }}">
            <tr>
                <td>
                    <input type="text" name="email" placeholder="E-mail" value="{{ (old('email', $contact->email)) }}">                    
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="region" placeholder="Регион" value="{{ (old('region', $contact->region)) }}">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="city" placeholder="Город" value="{{ (old('city', $contact->city)) }}">
                </td>
            </tr>
            <tr>
                <td>
            <tr>
                <td>
                    <input type="text" name="address" placeholder="Адрес" value="{{ (old('address', $contact->address)) }}">
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Изменить</button>
                </td>
            </tr>
        </form>
    </table>
</div>
</body>
</html>