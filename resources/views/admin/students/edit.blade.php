<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Студент
    </p>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table>
        <form action="{{ route('students_update', [$student->id]) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('put') }}
            <tr>
                <td>
                    <image src="{{ $student->getImage() }}" style="width: 100px; height: 100px;"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="image" name="image" type="file"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="name" placeholder="Имя" value="{{ old('name', $student->name) }}">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="surname" placeholder="Фамилия" value="{{ old('surname', $student->surname) }}">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="patronymic" placeholder="Отчество" value="{{ old('patronymic', $student->patronymic) }}">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="gradebook" placeholder="Номер зачетной книжки" value="{{ old('gradebook', $student->gradebook) }}">
                </td>
            </tr>
            <tr>
                <td>
                    <select name="group_id">
                        @if (isset($groups))
                            @foreach ($groups as $group)
                                {{--@if (old("group_id") == null)--}}
                                    <option {{  $student->group->id == $group->id ? 'selected' : '' }} value="{{  $group->id  }}">{{  $group->name  }}</option>
                                {{--@else--}}
                                    {{--<option {{  old("group_id") == $group->id ? 'selected' : '' }} value="{{  $group->id  }}">{{  $group->name  }}</option>--}}
                                {{--@endif--}}
                            @endforeach
                        @endif
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Изменить</button>
                </td>
            </tr>
        </form>
    </table>
    <a href="{{ route('contacts_edit', ['roleUser' => 'students', 'id' => $student->id]) }}">Контакты</a>
</div>
</body>
</html>