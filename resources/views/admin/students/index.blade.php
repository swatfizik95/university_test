<html>
<head>
    <title>Админ</title>
</head>
<body>

<div class="container">
    <p>
        Студенты
    </p>
    <table>
        <a href="{{ route('students_create') }}">Создать</a>
        @if(isset($students))
            @foreach ($students as $student)
                <tr>
                    <td>{{ $student->id }}</td>
                    <td>{{ $student->surname }}</td>
                    <td>{{ $student->name }}</td>
                    <td>{{ $student->patronymic }}</td>
                    <td>{{ $student->group->name }}</td>
                    <td>{{ $student->gradebook }}</td>
                    <td><img src="{{ $student->getSmallImage() }}" width="100" height="100"></td>
                    <td>
                        <a href="{{ route('students_edit', ['id' => $student->id]) }}">Редактировать</a>
                    </td>
                    <td>
                        <form action="{{ route('students_destroy', ['id' => $student->id]) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <button type="submit">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
</div>
</body>
</html>