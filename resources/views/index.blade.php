<!doctype html>
<html lang="ru">
<head>
    <title>Главная</title>
</head>
<body>

<div class="container">
    <p>
        Вы на главной
    </p>
    @auth
        <a href="{{ route('logout') }}">Выход</a>
        <script>
            window.user = @json(auth()->user())
        </script>
    @endauth
    @guest
        @if(session('status'))
            {{  session('status')  }}
        @endif
        <form action="{{ route('login') }}" role="form" method="post">
            {{  csrf_field()  }}
            <div class="form-group">
                <input type="text" name="name" placeholder="Имя" value="{{ old('name') }}">
            </div>
            <div class="form-group">
                <input type="text" name="surname" placeholder="Фамилия" value="{{ old('surname') }}">
            </div>
            <div class="form-group">
                <input type="text" name="patronymic" placeholder="Отчество" value="{{ old('patronymic') }}">
            </div>
            <div class="form-group">
                <input type="password" name="password" placeholder="Пароль">
            </div>
            <div class="form-group">
                <button type="submit">Войти</button>
            </div>
        </form>
    @endguest
</div>
</body>
</html>