<html>
<head>
    <title>Тест</title>
</head>
<body>

<div class="container">
    <p>
        Тест
    </p>
    <table>
        <form action="{{ route('test_store') }}" method="post">
            {{ csrf_field() }}
            <tr>
                <td>
                    <input type="text" name="name" placeholder="Имя" value="{{ old('name') }}">
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit">Отправить</button>
                </td>
            </tr>
        </form>
    </table>
</div>
</body>
</html>