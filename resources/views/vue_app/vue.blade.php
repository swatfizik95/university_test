@extends('master')
@section('content')
    
    <div id="app" class="ie-fix">
        <router-view></router-view>
    </div>

    <noscript>
        К сожалению, сайт не работает с выключенным JavaScript
    </noscript>

    @auth
        <script>
            window.user = @json(auth()->user());
        </script>
    @endauth

@endsection