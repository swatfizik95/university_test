<!doctype html>
<html lang="ru">
<head>
    <title>Главная</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>

    @yield('content')

    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>