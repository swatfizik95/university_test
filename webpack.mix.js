let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/css/style.scss', 'public/css/app.css')
    .options(
        {
            processCssUrls: false,
            postCss: [require('autoprefixer')],
        }
    )
    .browserSync({
        proxy: 'http://127.0.0.1:8000/vue/admin/',
        files: 'public/*',
    });

mix.js('resources/assets/js/app.js', 'public/js')
    .extract();

if (mix.inProduction())
{
    mix.version().sourceMaps();
}