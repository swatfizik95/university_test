<?php
/**
 * Created by PhpStorm.
 * User: swatfizik
 * Date: 04.02.2019
 * Time: 18:53
 */

namespace App\Traits;

use App\Models\Role;

trait HasAdminRole
{
    /**
     * HasAdminRole constructor.
     */
    public function __construct()
    {
        $this->middleware('apiRole:' . Role::ADMIN);
    }
}