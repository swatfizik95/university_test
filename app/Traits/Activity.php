<?php
/**
 * Created by PhpStorm.
 * User: swatfizik
 * Date: 04.02.2019
 * Time: 20:03
 */

namespace App\Traits;

trait Activity
{
    //region scope методы

    // активные записи
    /**
     * @param $query
     */
    public final static function scopeActive($query){
        $query->where('status_id', static::getActiveStatus());
    }

    // удаленные записи
    /**
     * @param $query
     */
    public final static function scopeNoActive($query){
        $query->where('status_id', static::getNoActiveStatus());
    }

    //endregion

    //region обычные методы

    /**
     * @return integer
     */
    public final static function getActiveStatus()
    {
        return Status::getId(Status::CODE_ACTIVE, static::TABLE);
    }

    /**
     * @return integer
     */
    public final static function getNoActiveStatus()
    {
        return Status::getId(Status::CODE_NOACTIVE, static::TABLE);
    }

    //endregion
}