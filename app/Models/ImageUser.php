<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageUser extends Image
{
    const TABLE = 'image_users';
    const NO_USER_IMAGE = '/images/no_user_image.png';
    const NO_USER_IMAGE_150x150 = '/images/no_user_image_150x150.png';

    public final static function scopeSize($query, int $code){
        $query->where('res_id', Status::getId($code, ImageUser::TABLE));
    }
}
