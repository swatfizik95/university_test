<?php

namespace App\Models;

class Role extends MainModel
{
    const TABLE = 'roles';

    const ADMIN = 1;
    const TEACHER = 10;
    const STUDENT = 11;

    public $timestamps = false;

    //region связи

    public function users(){
        return $this
            ->belongsToMany(User::class, RoleUser::TABLE, 'role_id', 'user_id')
            ->where(RoleUser::TABLE . '.status_id', Status::getId(Status::CODE_ACTIVE, RoleUser::TABLE))
            ->where(User::TABLE . '.status_id', Status::getId(Status::CODE_ACTIVE, User::TABLE));
    }

    //endregion

    //region обычные методы

    public static function getId(int $code){
        return Role::where('code', $code)->value('id') ?? null;
    }

//    public static function getIdByRoute($routeName){
//        return Role::where('route_name', $routeName)->value('id') ?? null;
//    }

    //endregion
}
