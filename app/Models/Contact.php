<?php

namespace App\Models;

class Contact extends MainModel
{
    const TABLE = 'contacts';

    protected $fillable =
        [
            'user_id',
            'email',
            'region',
            'city',
            'address',
            //'status_id'
        ];

    //region роли

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function phones()
    {
        return $this
            ->hasMany(Phone::class)
            ->where(Phone::TABLE . '.status_id', Phone::getActiveStatus());
    }

    //endregion

    /**
     * @param int $id
     * @return Contact
     */
    public static function getByUserId(int $id)
    {
        return Contact::active()
            ->where('user_id', $id)
            ->first();
    }

    public static function addOrModify($fields, $id)
    {
        $contact = Contact::getByUserId($id);

        if ($contact == null)
        {
            return Contact::add($fields);
        }
        else
        {
            return $contact->modify($fields);
        }
    }
}
