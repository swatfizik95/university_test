<?php

namespace App\Models;

/**
 * Class Group
 * @package App\Models
 */
class Group extends MainModel
{
    const TABLE = 'groups';

    protected $fillable =
        [
            'name', 'status_id'
        ];
}
