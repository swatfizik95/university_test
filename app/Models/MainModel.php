<?php
/**
 * Created by PhpStorm.
 * User: swatfizik95
 * Date: 09.12.2018
 * Time: 15:50
 */

namespace App\Models;

abstract class MainModel extends AbstractModel
{
    protected $hidden =
        [
            'created_at',
            'updated_at',
            'status_id'
        ];

    //region CRUD

    // create

    /**
     * @return static[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return static::active()->get();
    }

    /**
     * @param $fileds
     * @return static
     */
    public static function add($fileds)
    {
        $model = new static;
        $model->fill($fileds);
        $model->status_id = static::getActiveStatus();
        $model->save();

        return $model;
    }

    /**
     * @param int $id
     * @return static
     */
    public static function get(int $id){
        return static::active()->find($id);
    }

    /**
     * @param $fileds
     * @return void
     */
    public function modify($fileds){
        $this->fill($fileds);
        $this->save();
    }

    /**
     * @param $id
     * @return void
     */
    public static function remove($id){
        $model = static::get($id);
        $model->status_id = Status::getId(Status::CODE_NOACTIVE, static::TABLE);
        $model->save();
    }

    //endregion
}