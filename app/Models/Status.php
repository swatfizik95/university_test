<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const TABLE = 'statuses';

    // Стандартные коды статусов
    const CODE_ACTIVE = 1;
    const CODE_NOACTIVE = 2;

    // Размеры изображений
    const CODE_FULL = 30;
    const CODE_150x150 = 31;

    public $timestamps = false;

    protected $table = self::TABLE;

    /**
     * @param int $code
     * @param string $tableName
     * @return int|null
     */
    public static function getId(int $code, string $tableName){
        return Status::where([['code', $code], ['table', $tableName]])->value('id') ?? null;
    }
}
