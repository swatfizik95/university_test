<?php
/**
 * Created by PhpStorm.
 * User: swatfizik
 * Date: 26.12.2018
 * Time: 16:27
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractModel extends Model
{
    const TABLE = 'table';

    protected $table;

    /**
     * MainModel constructor.
     * @param $table
     */
    public function __construct()
    {
        $this->table = static::TABLE;
    }

    //region scope методы

    // активные записи
    /**
     * @param $query
     */
    public final static function scopeActive($query){
        $query->where('status_id', static::getActiveStatus());
    }

    // удаленные записи
    /**
     * @param $query
     */
    public final static function scopeNoActive($query){
        $query->where('status_id', static::getNoActiveStatus());
    }

    //endregion

    //region обычные методы

    /**
     * @return integer
     */
    public final static function getActiveStatus()
    {
        return Status::getId(Status::CODE_ACTIVE, static::TABLE);
    }

    /**
     * @return integer
     */
    public final static function getNoActiveStatus()
    {
        return Status::getId(Status::CODE_NOACTIVE, static::TABLE);
    }

    //endregion
}