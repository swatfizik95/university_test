<?php

namespace App\Models;

class Family extends MainModel
{
    const TABLE = 'families';

    protected $fillable =
        [
            'name',
            'surname',
            'patronymic',
            'phone',
            'address',
            'status_id'
        ];
}
