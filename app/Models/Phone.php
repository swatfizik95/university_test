<?php

namespace App\Models;

class Phone extends MainModel
{
    const TABLE = 'phones';

    protected $fillable =
        [
            'number',
            'status_id'
        ];
}
