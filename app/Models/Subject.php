<?php

namespace App\Models;

class Subject extends MainModel
{
    const TABLE = 'subjects';

    protected $fillable = ['name', 'semester_id', 'group_id', 'teacher_id'];

    //region связи

    public function group()
    {
        return $this
            ->hasOne(Group::class, 'id', 'group_id')
            ->where(Group::TABLE . '.status_id', Group::getActiveStatus());
    }

    public function teacher()
    {
        return $this
            ->hasOne(User::class, 'id', 'teacher_id')
            ->where(User::TABLE . '.status_id', User::getActiveStatus());
    }

    public function semester()
    {
        return $this
            ->hasOne(Semester::class, 'id', 'semester_id');
    }

    //endregion
}
