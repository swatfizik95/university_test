<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    const TABLE = 'semesters';

    public $timestamps = false;

    protected $table = self::TABLE;
}
