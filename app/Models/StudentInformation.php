<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentInformation extends MainModel
{
    const TABLE = 'student_informations';

    protected $fillable = ['gradebook', 'user_id', 'group_id', 'status_id'];

    //region связи

    public function group()
    {
        return $this
            ->belongsTo(Group::class, 'group_id')
            ->where(Group::TABLE . '.status_id', Group::getActiveStatus());
    }

    //endregion
}
