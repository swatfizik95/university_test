<?php

namespace App\Models;

abstract class Image extends AbstractModel
{
    public $timestamps = false;

    /**
     * @param string $url
     * @param int $code
     * @return Image
     */
    public static function make(string $url, int $code)
    {
        $image = new static;
        $image->url = $url;
        $image->status_id = static::getActiveStatus();
        $image->res_id = Status::getId($code, static::TABLE);

        return $image;
    }
}
