<?php

namespace App\Models;

use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as ImageMager;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 * @package App\Models
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const TABLE = 'users';

    protected $table = self::TABLE;

    protected $fillable = [
        'name',
        'surname',
        'patronymic', /*'email',*/
        'status_id'
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'status_id',
        'created_at',
        'updated_at',
    ];

    //region связи

    public function roles()
    {
        return $this
            ->belongsToMany(Role::class, RoleUser::TABLE, 'user_id', 'role_id')
            ->where(RoleUser::TABLE . '.status_id', RoleUser::getActiveStatus())
            ->where(Role::TABLE . '.status_id', Role::getActiveStatus());
    }

    public function studentInformation()
    {
        return $this
            ->hasOne(StudentInformation::class, 'user_id')
            ->where(StudentInformation::TABLE . '.status_id', StudentInformation::getActiveStatus());
    }

    public function smallImage()
    {
        return $this
            ->hasOne(ImageUser::class, 'user_id')
            ->where(ImageUser::TABLE . '.status_id', ImageUser::getActiveStatus())
            ->where(ImageUser::TABLE . '.res_id', Status::getId(Status::CODE_150x150, ImageUser::TABLE));
    }

    public function image()
    {
        return $this
            ->hasOne(ImageUser::class, 'user_id')
            ->where(ImageUser::TABLE . '.status_id', ImageUser::getActiveStatus())
            ->where(ImageUser::TABLE . '.res_id', Status::getId(Status::CODE_FULL, ImageUser::TABLE));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contact()
    {
        return $this
            ->hasOne(Contact::class, 'user_id')
            ->where(Contact::TABLE . '.status_id', Contact::getActiveStatus());
    }

    public function progresses()
    {
        return $this
            ->hasMany(Progress::class, 'student_id')
            ->where(Progress::TABLE . '.status_id', Progress::getActiveStatus());
    }

    //endregion

    //region scope методы
    public function scopeActive($query)
    {
        return $query->where('status_id', Status::getId(Status::CODE_ACTIVE, User::TABLE));
    }

    // всех по ролям
    public function scopeRole($query, $role)
    {
        return $query->whereHas(Role::TABLE, function ($query) use ($role) {
            $query->where(Role::TABLE . '.id', Role::getId($role));
        });
    }

    // всех студентов
    public function scopeStudent($query)
    {
        return $this->scopeRole($query, Role::STUDENT);
    }
    //endregion

    //region мутаторы и аксессоры

    public function getGroupAttribute()
    {
        return $this->studentInformation->group;
    }

    /**
     * @return string
     */
    public function getGradebookAttribute()
    {
        return $this->studentInformation->gradebook;
    }

    public function setGradebookAttribute($value)
    {
        $this->studentInformation->attributes['gradebook'] = $value;
    }

    //endregion

    //region обычные методы

    // получаем активный статус
    public static function getActiveStatus()
    {
        return Status::getId(Status::CODE_ACTIVE, self::TABLE);
    }

    // получаем неактивный статус
    public static function getNoActiveStatus()
    {
        return Status::getId(Status::CODE_NOACTIVE, self::TABLE);
    }

    // проверка по роли
    public function isRole(int $role)
    {
        return $this
                ->roles()
                ->where(Role::TABLE . '.id', Role::getId($role))
                ->first() != null;
    }

    // проверка активен ли
    public function isActive()
    {
        return $this->status_id == self::getActiveStatus();
    }

    // добавляем роль
    public function addRole(int $role)
    {
        $this->roles()->attach([Role::getId($role) => ['status_id' => RoleUser::getActiveStatus()]]);
    }

    // добавляем изображения
    /**
     * @param UploadedFile $image
     */
    public function addImage($file)
    {
        if ($file == null)
            return;

        $image = ImageMager::make($file);

        $url = '/images/user_image_full' . uniqid() . '.' . $file->getClientOriginalExtension();
        $image->save(public_path() . $url);
        $imageUser = ImageUser::make($url, Status::CODE_FULL);
        $this->image()->save($imageUser);

        $url = '/images/user_image_150x150' . uniqid() . '.' . $file->getClientOriginalExtension();
        $image->resize(150, 150)->save(public_path() . $url);
        $imageUser = ImageUser::make($url, Status::CODE_150x150);
        $this->smallImage()->save($imageUser);
    }

    public function getImage()
    {
        $image = ImageUser::active()->size(Status::CODE_FULL)->where('user_id', $this->id)->value('url');
        return $image == null ? ImageUser::NO_USER_IMAGE : $image;
    }

    public function getSmallImage()
    {
        $image = ImageUser::active()->size(Status::CODE_150x150)->where('user_id', $this->id)->value('url');
        return $image == null ? ImageUser::NO_USER_IMAGE_150x150 : $image;
    }

    public function modifyImage($file)
    {
        if ($file == null)
            return;

        if ($this->image != null)
        {
            $this->image->status_id = ImageUser::getNoActiveStatus();
            $this->image->save();
        }
        if ($this->smallImage != null)
        {
            $this->smallImage->status_id = ImageUser::getNoActiveStatus();
            $this->smallImage->save();
        }

        $this->addImage($file);
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->surname . ' ' . $this->name . ' ' . $this->patronymic;
    }

    //endregion

    //region CRUD

    /**
     * @param int $id
     * @return User
     */
    public static function get(int $id)
    {
        return User::active()->find($id);
    }

    /**
     * @param $name
     * @return User
     */
    public static function getByName($name)
    {
        return User::active()->where('name', $name)->first();
    }

    //endregion

    //region CRUD студентов

    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getStudentAll()
    {
        return User::active()
            ->role(Role::STUDENT)
            ->get();
    }

    /**
     * @param $fields
     * @return User
     */
    public static function addStudent($fields, $file)
    {
        $user = new static;
        $user->fill($fields);
        $user->password = bcrypt($fields['gradebook']);
        $user->status_id = self::getActiveStatus();
        $user->save();

        $studentInformation = new StudentInformation;
        $studentInformation->fill($fields);
        $studentInformation->status_id = StudentInformation::getActiveStatus();
        $user->studentInformation()->save($studentInformation);
        $user->addRole(Role::STUDENT);

        $user->addImage($file);

        return $user;
    }

    /**
     * @param int $id
     * @return User
     */
    public static function getStudent(int $id)
    {
        return User::active()
            ->role(Role::STUDENT)
            ->find($id);
    }

    /**
     * @param $fields
     */
    public function modifyStudent($fields, $file)
    {
        $this->fill($fields);
        if ($this->gradebook != $fields['gradebook'])
        {
            $this->gradebook = $fields['gradebook'];
            $this->password = bcrypt($fields['gradebook']);
        }
        $this->studentInformation->group_id = $fields['group_id'];
        $this->studentInformation->save();

        $this->modifyImage($file);

        $this->save();
    }

    /**
     * @param $id
     * @return void
     */
    public static function removeStudent($id)
    {
        $student = User::getStudent($id);
        if ($student == null) return;
        $student->status_id = self::getNoActiveStatus();
        $student->save();
    }

    //endregion

    //region CRUD учителей

    /**
     * @return User[]
     */
    public static function getTeacherAll()
    {
        return User::active()
            ->role(Role::TEACHER)
            ->get();
    }

    /**
     * @param $fields
     * @return User
     */
    public static function addTeacher($fields, $file)
    {
        $teacher = new static;
        $teacher->fill($fields);
        $teacher->password = bcrypt($fields['password']);
        $teacher->status_id = self::getActiveStatus();
        $teacher->save();

        $teacher->addRole(Role::TEACHER);

        $teacher->addImage($file);

        return $teacher;
    }

    /**
     * @param int $id
     * @return User
     */
    public static function getTeacher(int $id)
    {
        return User::active()
            ->role(Role::TEACHER)
            ->find($id);
    }

    /**
     * @param $fields
     */
    public function modifyTeacher($fields, $file)
    {
        $this->fill($fields);
        if ($fields['password'] != null)
        {
            $this->password = bcrypt($fields['password']);
        }

        $this->modifyImage($file);

        $this->save();
    }

    public static function removeTeacher(int $id)
    {
        $teacher = User::getTeacher($id);
        $teacher->status_id = self::getNoActiveStatus();
        $teacher->save();
    }

    //endregion
}
