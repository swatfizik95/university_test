<?php

namespace App\Models;

class RoleUser extends MainModel
{
    const TABLE = 'role_user';

    public $timestamps = false;

    protected $fillable = [
        'role_id',
        'user_id',
        'status_id'
    ];
}
