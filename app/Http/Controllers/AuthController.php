<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::attempt([
            'name' => $request->get('name'),
            //'surname' => $request->get('surname'),
            //'patronymic' => $request->get('patronymic'),
            'password' => $request->get('password'),
            'status_id' => User::getActiveStatus()
        ]))
        {
            if (Auth::user()->isRole(Role::ADMIN))
            {
                return redirect()->route('admin_index');
            }
            if (Auth::user()->isRole(Role::STUDENT))
            {
                return redirect()->route('students_index');
            }
            if (Auth::user()->isRole(Role::TEACHER))
            {
                return redirect()->route('teacher_index');
            }

            return redirect('/');
        }

        return redirect()->back()->withInput()->with('status', 'Неправильный логин или пароль');
    }

    public function loginShowTest()
    {
        return view('loginTest');
    }

    public function loginTest(Request $request)
    {
        if (Auth::attempt([
            'name' => $request->get('name'),
            //'surname' => $request->get('surname'),
            //'patronymic' => $request->get('patronymic'),
            'password' => $request->get('password'),
            'status_id' => User::getActiveStatus()
        ]))
        {
            return response()->json(
                [
                    'id' => Auth::user()->id,
                    'name' => Auth::user()->name,
                    'roles' => Auth::user()->roles()->select('name', 'code')->pluck('name', 'code'),
                    'cookies' => $request->cookies
                ], 200, [], JSON_UNESCAPED_UNICODE);
        }

        return redirect()->back()->withInput()->with('status', 'Неправильный логин или пароль');
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }
}
