<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        return response()->json(['status' => 'OK', 'request' => $request->all()]);;
    }
}
