<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ApiAuthController extends Controller
{
    public function login(Request $request)
    {
        $user = User::getByName($request->name);
        if ($user != null)
        {
            if (Hash::check($request->password, User::where('id', $user->id)->value('password')))
            {
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;

                $token->expires_at = Carbon::now()->addMinute(1);
                $token->save();

                return $tokenResult;
            }

            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        return response()->json([
            'message' => 'Unauthorized'
        ], 401);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ], 200);
    }
}
