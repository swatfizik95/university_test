<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\Admin\StudentPostRequest;
use App\Models\Group;
use App\Models\User;
use App\Traits\HasAdminRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiAdminStudentController extends Controller
{
    use HasAdminRole;

    public function index()
    {
        $users = User::getStudentAll();
        foreach ($users as $user)
        {
            $user->img = $user->getSmallImage();
        }

        return response()->json(['students' => $users]);
    }

    public function create()
    {
        return response()->json(['groups' => Group::getAll()]);
    }

    public function store(StudentPostRequest $request)
    {
        User::addStudent($request->all(), $request->file('image'));

        return response()->json(['status' => 'Ok']);
    }

    public function edit($id)
    {
        $student = User::getStudent($id);
        $student->img = $student->getImage();
        if ($student == null)
            return response()->json(['message' => 'Student not found'], 404);

        return response()->json(['student' => $student, 'groups' => Group::getAll()]);
    }

    public function update(StudentPostRequest $request, $id)
    {
        $student = User::getStudent($id);
        if ($student == null)
            return response()->json(['message' => 'Student not found'], 404);

        $student->modifyStudent($request->all(), $request->file('image'));

        return response()->json(['status' => 'Ok']);
    }

    public function destroy($id)
    {
        User::removeStudent($id);

        return response()->json(['status' => 'Ok']);
    }
}
