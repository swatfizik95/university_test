<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\Admin\TeacherPostRequest;
use App\Http\Requests\Admin\TeacherPutRequest;
use App\Models\User;
use App\Traits\HasAdminRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiAdminTeacherController extends Controller
{
    use HasAdminRole;

    public function index()
    {
        $teachers = User::getTeacherAll();
        foreach ($teachers as $teacher)
        {
            $teacher->img = $teacher->getSmallImage();
        }

        return response()->json(['teachers' => $teachers]);
    }

    public function store(TeacherPostRequest $request)
    {
        User::addTeacher($request->all(), $request->file('image'));

        return response()->json(['status' => 'Ok']);
    }

    public function edit(int $id)
    {
        $teacher = User::getTeacher($id);
        $teacher->img = $teacher->getImage();

        if ($teacher == null)
            return response()->json(['message' => 'Teacher not found'], 404);

        return response()->json(['teacher' => $teacher]);
    }

    public function update(TeacherPutRequest $request, int $id)
    {
        $teacher = User::getTeacher($id);

        if ($teacher == null)
            return response()->json(['message' => 'Teacher not found'], 404);

        $teacher->modifyTeacher($request->all(), $request->file('image'));

        return response()->json(['status' => 'Ok']);
    }

    public function destroy(int $id)
    {
        User::removeTeacher($id);

        return response()->json(['status' => 'Ok']);
    }
}
