<?php

namespace App\Http\Controllers\Api\Admin;

use App\Traits\HasAdminRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiAdminProgressController extends Controller
{
    use HasAdminRole;
}
