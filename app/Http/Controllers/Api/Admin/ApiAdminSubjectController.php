<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\Admin\SubjectPostRequest;
use App\Models\Group;
use App\Models\Semester;
use App\Models\Subject;
use App\Models\User;
use App\Traits\HasAdminRole;
use App\Http\Controllers\Controller;

class ApiAdminSubjectController extends Controller
{
    use HasAdminRole;

    public function index()
    {
        return response()->json(['subhects' => Subject::getAll()]);
    }

    public function create()
    {
        return response()->json([
            'semesters'     => Semester::all(),
            'groups'        => Group::getAll(),
            'teachers'      => User::getTeacherAll()
        ]);
    }

    public function store(SubjectPostRequest $request)
    {
        Subject::add($request->all());

        return response()->json(['status' => 'Ok']);
    }

    public function edit(int $id)
    {
        $subject = Subject::get($id);

        if ($subject == null)
            return response()->json(['message' => 'Subject not found'], 404);

        return response()->json([
            'subject'       => $subject,
            'semesters'     => Semester::all(),
            'groups'        => Group::getAll(),
            'teachers'      => User::getTeacherAll()
        ]);
    }

    public function update(SubjectPostRequest $request, int $id)
    {
        $subject = Subject::get($id);
        if ($subject == null)
            return response()->json(['message' => 'Subject not found'], 404);

        $subject->modify($request->all());

        return response()->json(['status' => 'Ok']);
    }

    public function destroy(int $id)
    {
        Subject::remove($id);

        return response()->json(['status' => 'Ok']);
    }
}
