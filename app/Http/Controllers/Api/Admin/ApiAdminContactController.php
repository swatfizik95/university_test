<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\Admin\ContactPostRequest;
use App\Models\Contact;
use App\Traits\HasAdminRole;
use App\Traits\TestTrait;
use App\Http\Controllers\Controller;

class ApiAdminContactController extends Controller
{
    use HasAdminRole;

    public function edit($roleUser, $id)
    {
        $contact = Contact::getByUserId($id) ?? new Contact();

        return response()->json([
            'roleUser' => $roleUser,
            'id' => $id,
            'contact' => $contact,
        ]);
    }

    public function update(ContactPostRequest $request, $roleUser, $id)
    {
        Contact::addOrModify($request->all(), $id);

        return response()->json(['status' => 'Ok']);
    }
}
