<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\Admin\GroupPostRequest;
use App\Models\Group;
use App\Traits\HasAdminRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiAdminGroupController extends Controller
{
    use HasAdminRole;

    public function index()
    {
        return response()->json(
            [
                'groups' => Group::getAll()
            ]);
    }

    public function store(GroupPostRequest $request)
    {
        Group::add($request->all());

        return response()->json(['status' => 'Ok']);
    }

    public function edit(int $id)
    {
        $group = Group::get($id);
        if ($group == null)
            return response()->json(['message' => 'Group not founded'], 404);

        return response()->json(['group' => $group]);
    }

    public function update(GroupPostRequest $request, int $id)
    {
        $group = Group::get($id);

        if ($group == null)
            return response()->json(['message' => 'Group not founded'], 404);

        $group->modify($request->all());

        return response()->json(['status' => 'Ok']);
    }

    public function destroy($id)
    {
        Group::remove($id);

        return response()->json(['status' => 'Ok']);
    }
}
