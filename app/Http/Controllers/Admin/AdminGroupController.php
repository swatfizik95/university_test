<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\GroupPostRequest;
use App\Models\Group;
use App\Http\Controllers\Controller;

class AdminGroupController extends Controller
{
    const VIEWPATH = 'admin.groups';

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view(self::VIEWPATH . '.index', ['groups' => Group::active()->get()]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view(self::VIEWPATH . '.create');
    }

    /**
     * @param GroupPostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(GroupPostRequest $request)
    {
        Group::add($request->all());

        return redirect()->route('groups_list');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        return view(self::VIEWPATH . '.edit', ['group' => Group::get($id) ?? abort('404')]);
    }

    /**
     * @param GroupPostRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(GroupPostRequest $request, $id)
    {
        $group = Group::get($id) ?? abort('404');
        $group->modify($request->all());

        return redirect()->route('groups_list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Group::remove($id);

        return redirect()->route('groups_list');
    }
}
