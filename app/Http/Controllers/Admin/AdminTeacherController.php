<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\TeacherPostRequest;
use App\Http\Requests\Admin\TeacherPutRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminTeacherController extends Controller
{
    const VIEWPATH = 'admin.teachers';

    public function __construct() {
        //$this->middleware('role:' . Role::ADMIN);
    }

    public function index()
    {
        $teachers = User::getTeacherAll();

        return view(self::VIEWPATH . '.index', ['teachers' => $teachers]);
    }

    public function create()
    {
        return view(self::VIEWPATH . '.create');
    }

    public function store(TeacherPostRequest $request)
    {
        User::addTeacher($request->all(), $request->file('image'));

        return redirect()->route('teachers_list');
    }

    public function edit(int $id)
    {
        $teacher = User::getTeacher($id) ?? abort('404');

        return view(self::VIEWPATH . '.edit', ['teacher' => $teacher]);
    }

    public function update(TeacherPutRequest $request, int $id)
    {
        $teacher = User::getTeacher($id) ?? abort('404');
        $teacher->modifyTeacher($request->all(), $request->file('image'));

        return redirect()->route('teachers_list');
    }

    public function destroy(int $id)
    {
        User::removeTeacher($id);

        return redirect()->route('teachers_list');
    }
}
