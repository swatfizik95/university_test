<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminFallbackController extends Controller
{
    public function fallback(Request $request) {
        if ($request->ajax()) {
            return response()->json([], 404);
        }

        return view('vue_app.vue');
    }
}
