<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SubjectPostRequest;
use App\Models\Group;
use App\Models\Role;
use App\Models\Semester;
use App\Models\Subject;
use App\Models\User;
use App\Http\Controllers\Controller;

class AdminSubjectController extends Controller
{
    const VIEWPATH = 'admin.subjects';

    /**
     * AdminSubjectController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:' . Role::ADMIN);
    }

    public function index()
    {
        $subjects = Subject::getAll();

        return view(self::VIEWPATH . '.index', ['subjects' => $subjects]);
    }

    public function create()
    {
        return view(self::VIEWPATH . '.create',
            [
                'semesters'     => Semester::all(),
                'groups'        => Group::getAll(),
                'teachers'      => User::getTeacherAll()
            ]);
    }

    public function store(SubjectPostRequest $request)
    {
        Subject::add($request->all());

        return redirect()->route('subjects_list');
    }

    public function edit(int $id)
    {
        return view(self::VIEWPATH . '.edit',
            [
                'subject' => Subject::get($id) ?? abort(404),
                'semesters'     => Semester::all(),
                'groups'        => Group::getAll(),
                'teachers'      => User::getTeacherAll()
            ]);
    }

    public function update(SubjectPostRequest $request, int $id)
    {
        $subject = Subject::get($id) ?? abort(404);
        $subject->modify($request->all());

        return redirect()->route('subjects_list');
    }

    public function destroy(int $id)
    {
        Subject::remove($id);

        return redirect()->route('subjects_list');
    }
}