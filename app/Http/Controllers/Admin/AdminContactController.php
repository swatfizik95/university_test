<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ContactPostRequest;
use App\Models\Contact;
use App\Http\Controllers\Controller;
use App\Models\Role;

class AdminContactController extends Controller
{
    const VIEWPATH = 'admin.contacts';

    /**
     * ProgressController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:' . Role::ADMIN);
    }

    public function edit($roleUser, $id)
    {
        $contact = Contact::getByUserId($id) ?? new Contact();
        return view(self::VIEWPATH . '.edit', compact('roleUser', 'id', 'contact'));
    }

    public function update(ContactPostRequest $request, $userRole, int $id)
    {
        Contact::addOrModify($request->all(), $id);

        return redirect()->route($userRole . '_edit', ['id' => $id]);
    }
}
