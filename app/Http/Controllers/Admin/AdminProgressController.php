<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminProgressController extends Controller
{
    const VIEWPATH = 'admin.progresses';

    /**
     * ProgressController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:' . Role::ADMIN);
    }

    public function index()
    {
        dd('index');
        return view(self::VIEWPATH . '.index');
    }
}
