<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StudentPostRequest;
use App\Http\Requests\Admin\StudentPutRequest;
use App\Models\Group;
use App\Models\Role;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class AdminStudentController extends Controller
{
    const VIEWPATH = 'admin.students';

    public function __construct() {
        $this->middleware('role:' . Role::ADMIN);
    }

    public function index()
    {
        return view(self::VIEWPATH . '.index', ['students' => User::getStudentAll()]);
    }

    public function create()
    {
        return view(self::VIEWPATH . '.create', ['groups' => Group::getAll()]);
    }

    public function store(StudentPostRequest $request)
    {
        User::addStudent($request->all(), $request->file('image'));

        return redirect()->route('students_list');
    }

    public function edit($id)
    {
        $student = User::getStudent($id) ?? abort('404');

        return view(self::VIEWPATH . '.edit', ['student' => $student, 'groups' => Group::getAll()]);
    }

    public function update(StudentPostRequest $request, $id)
    {
        $student = User::getStudent($id) ?? abort('404');
        $student->modifyStudent($request->all(), $request->file('image'));

        return redirect()->route('students_list');
    }

    public function destroy($id)
    {
        User::removeStudent($id);

        return redirect()->route('students_list');
    }
}
