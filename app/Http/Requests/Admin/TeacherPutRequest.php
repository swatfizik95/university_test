<?php
/**
 * Created by PhpStorm.
 * User: swatfizik95
 * Date: 12.12.2018
 * Time: 15:47
 */

namespace App\Http\Requests\Admin;


class TeacherPutRequest extends MainRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|min:3|max:7|regex:/^[а-я\-\s\_]+$/mui',
            'surname'           => 'required|min:3|max:190|regex:/^[а-я\-\s\_]+$/mui',
            'patronymic'        => 'required|min:3|max:190|regex:/^[а-я\-\s\_]+$/mui',
            'password'          => 'nullable|min:6|max:190|regex:/^\w+$/mi'
        ];
    }
}