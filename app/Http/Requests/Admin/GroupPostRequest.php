<?php

namespace App\Http\Requests\Admin;

class GroupPostRequest extends MainRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'имя группы'
        ];
    }
}