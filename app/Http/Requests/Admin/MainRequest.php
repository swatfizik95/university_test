<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class MainRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [
            '*.required' => 'Поле :attribute обязательно для заполнения',
            '*.min' => 'Поле :attribute должно быть больше :min символов',
            '*.max' => 'Поле :attribute не должно быть больше :max символов',
            '*.regex' => 'Поле :attribute имеет некорректный формат',
            '*.digits_between' => 'Поле :attribute должно быть от :min до :max цифр',
            '*.email' => 'Поле :attribute не соответствует формату email почты (test-example@mail.ru)',
            '*.unique' => 'Поле :attribute уже существует',
            '*.image' => 'Поле :attribute должен иметь формат изображения'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'имя',
            'surname' => 'фамилия',
            'patronymic' => 'отчество',
            'password' => 'пароль',
            'image' => 'изображение',
            'gradebook' => 'номер зачетной книжки',
            'email' => 'почта',
            'region' => 'регион',
            'city' => 'город',
            'address' => 'адрес',
            'group_id' => 'группа',
            'semester_id' => 'семестр',
            'teacher_id' => 'учитель',
        ];
    }
}
