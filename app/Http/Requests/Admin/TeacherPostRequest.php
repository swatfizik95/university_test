<?php

namespace App\Http\Requests\Admin;


class TeacherPostRequest extends MainRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|min:3|max:7|regex:/^[а-я\-\s\_]+$/mui',
            'surname'           => 'required|min:3|max:190|regex:/^[а-я\-\s\_]+$/mui',
            'patronymic'        => 'required|min:3|max:190|regex:/^[а-я\-\s\_]+$/mui',
            'password'          => 'required|min:6|max:190|regex:/^\w+$/mi'
        ];
    }
}