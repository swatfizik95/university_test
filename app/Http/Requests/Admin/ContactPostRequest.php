<?php
/**
 * Created by PhpStorm.
 * User: swatfizik95
 * Date: 15.12.2018
 * Time: 15:22
 */

namespace App\Http\Requests\Admin;


use App\Models\Contact;
use Illuminate\Validation\Rule;

class ContactPostRequest extends MainRequest
{
    public function rules()
    {
        return
            [
                'email' => ['nullable', 'email', Rule::unique(Contact::TABLE, 'email')
                    ->ignore(Contact::getByUserId($this->id)->id ?? 0)],
                'region' => 'required',
                'city' => 'required',
                'address' => 'required',
            ];
    }
}