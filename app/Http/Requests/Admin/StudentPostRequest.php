<?php

namespace App\Http\Requests\Admin;

class StudentPostRequest extends MainRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|min:3|max:7|regex:/^[а-я\-\s\_]+$/mui',
            'image'             => 'sometimes|image',
            'surname'           => 'required|min:3|max:190|regex:/^[а-я\-\s\_]+$/mui',
            'patronymic'        => 'required|min:3|max:190|regex:/^[а-я\-\s\_]+$/mui',
            'gradebook'         => 'required|digits_between:5,15',
            'group_id'          => 'required'
        ];
    }
}
