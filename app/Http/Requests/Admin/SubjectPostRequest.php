<?php
/**
 * Created by PhpStorm.
 * User: swatfizik95
 * Date: 12.12.2018
 * Time: 17:11
 */

namespace App\Http\Requests\Admin;


class SubjectPostRequest extends MainRequest
{
    public function rules()
    {
        return
            [
                'name' => 'required|min:3|max:190',
                'semester_id' => 'required',
                'group_id' => 'required',
                'teacher_id' => 'required'
            ];
    }
}