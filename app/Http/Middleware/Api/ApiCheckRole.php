<?php

namespace App\Http\Middleware\Api;

use Closure;

class ApiCheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, int $roleCode)
    {
        if ($request->user()->isRole($roleCode))
        {
            return $next($request);
        }

        return response()->json([
            'message' => 'No rights'
        ], 401);
    }
}
