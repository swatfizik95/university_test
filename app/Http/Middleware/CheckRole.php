<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, int $roleCode)
    {
        if (Auth::check() && Auth::user()->isActive() && Auth::user()->isRole($roleCode))
        {
            return $next($request);
        }

        return abort(404);
    }
}
