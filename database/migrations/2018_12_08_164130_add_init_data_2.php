<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInitData2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table(\App\Models\StudentInformation::TABLE)->insert(array(
            [
                'group_id' => '1',
                'user_id' => '2',
                'gradebook' => '11',
                'status_id' => \App\Models\Status::getId(\App\Models\Status::CODE_ACTIVE, \App\Models\StudentInformation::TABLE)
            ],
            [
                'group_id' => '1',
                'user_id' => '3',
                'gradebook' => '12',
                'status_id' => \App\Models\Status::getId(\App\Models\Status::CODE_ACTIVE, \App\Models\StudentInformation::TABLE)
            ],
            [
                'group_id' => '2',
                'user_id' => '4',
                'gradebook' => '13',
                'status_id' => \App\Models\Status::getId(\App\Models\Status::CODE_ACTIVE, \App\Models\StudentInformation::TABLE)
            ]
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
