<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Status;
use App\Models\Role;

class AddInitData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('statuses')->insert(array(
            [
                'description' => 'пользователь активен',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\User::TABLE
            ],
            [
                'description' => 'пользователь не активен',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\User::TABLE
            ],
            [
                'description' => 'роль активна',
                'code' => Status::CODE_ACTIVE,
                'table' => Role::TABLE
            ],
            [
                'description' => 'роль не активна',
                'code' => Status::CODE_NOACTIVE,
                'table' => Role::TABLE
            ],
            [
                'description' => 'связка роль и пользователь активна',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\RoleUser::TABLE
            ],
            [
                'description' => 'связка роль и пользователь не активна',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\RoleUser::TABLE
            ],
            [
                'description' => 'контакт активен',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\Contact::TABLE
            ],
            [
                'description' => 'контакт не активен',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\Contact::TABLE
            ],
            [
                'description' => 'телефон активен',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\Phone::TABLE
            ],
            [
                'description' => 'телефон не активен',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\Phone::TABLE
            ],
            [
                'description' => 'группа активна',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\Group::TABLE
            ],
            [
                'description' => 'группа не активна',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\Group::TABLE
            ],
            [
                'description' => 'информацция о студенте активна',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\StudentInformation::TABLE
            ],
            [
                'description' => 'информацция о студенте не активна',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\StudentInformation::TABLE
            ],
            [
                'description' => 'предмет активен',
                'code' => Status::CODE_ACTIVE,
                'table' => \App\Models\Subject::TABLE
            ],
            [
                'description' => 'предмет не активен',
                'code' => Status::CODE_NOACTIVE,
                'table' => \App\Models\Subject::TABLE
            ],
            [
                'description' => 'успеваемость активна',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\Progress::TABLE
            ],
            [
                'description' => 'успеваемость не активна',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\Progress::TABLE
            ],
            [
                'description' => 'изображение пользователя активно',
                'code' => \App\Models\Status::CODE_ACTIVE,
                'table' => \App\Models\ImageUser::TABLE
            ],
            [
                'description' => 'изображение пользователя не активно',
                'code' => \App\Models\Status::CODE_NOACTIVE,
                'table' => \App\Models\ImageUser::TABLE
            ],
            [
                'description' => 'изображение пользователя с полным разрешением',
                'code' => \App\Models\Status::CODE_FULL,
                'table' => \App\Models\ImageUser::TABLE
            ],
            [
                'description' => 'изображение пользователя с разрешением 150 x 150',
                'code' => \App\Models\Status::CODE_150x150,
                'table' => \App\Models\ImageUser::TABLE
            ],
        ));

        DB::table('roles')->insert(array(
            [
                'name' => 'админ',
                'route_name' => null,
                'code' => Role::ADMIN,
                'status_id' => Status::getId(Status::CODE_ACTIVE, Role::TABLE),
                'description' => 'суперадмин'
            ],
            [
                'name' => 'учитель',
                'route_name' => 'teachers',
                'code' => Role::TEACHER,
                'status_id' => Status::getId(Status::CODE_ACTIVE, Role::TABLE),
                'description' => 'преподаватели'
            ],
            [
                'name' => 'студент',
                'route_name' => 'students',
                'code' => \App\Models\Role::STUDENT,
                'status_id' => Status::getId(Status::CODE_ACTIVE, Role::TABLE),
                'description' => 'студенты'
            ]
        ));

        DB::table('users')->insert(array(
            [
                'name' => 'admin',
                'surname' => '',
                'patronymic' => '',
                'password' => bcrypt('123'),
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\User::TABLE)
            ],
            [
                'name' => 'Вугар',
                'surname' => 'Панахов',
                'patronymic' => 'Садяр оглы',
                'password' => bcrypt('123'),
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\User::TABLE)
            ],
            [
                'name' => 'Сакит',
                'surname' => 'Алиев',
                'patronymic' => 'Махаммадалиевич',
                'password' => bcrypt('123'),
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\User::TABLE)
            ],
            [
                'name' => 'Расул',
                'surname' => 'Гаджиалиханов',
                'patronymic' => 'Мурадович',
                'password' => bcrypt('123'),
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\User::TABLE)
            ],
            [
                'name' => 'Наима',
                'surname' => 'Раджабова',
                'patronymic' => 'Шамильевна',
                'password' => bcrypt('123'),
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\User::TABLE)
            ],
            [
                'name' => 'Рамазан',
                'surname' => 'Кадиев',
                'patronymic' => 'Исмаилович',
                'password' => bcrypt('123'),
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\User::TABLE)
            ],
        ));

        DB::table('role_user')->insert(array(
            [
                'role_id' => 1,
                'user_id' => 1,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\RoleUser::TABLE)
            ],
            [
                'role_id' => 3,
                'user_id' => 2,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\RoleUser::TABLE)
            ],
            [
                'role_id' => 3,
                'user_id' => 3,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\RoleUser::TABLE)
            ],
            [
                'role_id' => 3,
                'user_id' => 4,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\RoleUser::TABLE)
            ],
            [
                'role_id' => 2,
                'user_id' => 5,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\RoleUser::TABLE)
            ],
            [
                'role_id' => 2,
                'user_id' => 6,
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\RoleUser::TABLE)
            ],
        ));

        DB::table('contacts')->insert(array(
            [
                'user_id' => 2,
                'email' => 'test2@mail.ru',
                'region' => 'москвоская область',
                'city' => 'москва',
                'address' => 'ул. Ленина 8, кв. 2',
                'status_id' => Status::getId(Status::CODE_NOACTIVE, \App\Models\Contact::TABLE)
            ],
            [
                'user_id' => 2,
                'email' => 'test@mail.ru',
                'region' => 'москвоская область',
                'city' => 'махачкала',
                'address' => 'ул. Ленина 8, кв. 2',
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Contact::TABLE)
            ]
        ));

        DB::table('phones')->insert(array(
            [
                'contact_id' => '2',
                'number' => '79634209180',
                'status_id' => Status::getId(Status::CODE_NOACTIVE, \App\Models\Phone::TABLE)
            ],
            [
                'contact_id' => '2',
                'number' => '79285106967',
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Phone::TABLE)
            ],
            [
                'contact_id' => '2',
                'number' => '79993112600',
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Phone::TABLE)
            ]
        ));

        DB::table('groups')->insert(array(
            [
                'name' => 'ФИиИТ17',
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Group::TABLE)
            ],
            [
                'name' => 'ПМиИ17',
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Group::TABLE)
            ],
            [
                'name' => 'МиКН17',
                'status_id' => Status::getId(Status::CODE_ACTIVE, \App\Models\Group::TABLE)
            ]
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
