<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//region main

Route::group(['prefix' => 'test', 'as' => 'test_'], function (){
    Route::get('/', 'TestController@create')->name('create');
    Route::post('/', 'TestController@store')->name('store');;
});

Route::get('/', function () {
    return view('index');
});

Route::group(['prefix' => 'admin', 'middleware' => 'role:' . \App\Models\Role::ADMIN, 'namespace' => 'Admin'], function (){
    Route::get('/', 'AdminController@index')->name('admin_index');

    // crud студентов
    Route::group(['prefix' => 'students', 'as' => 'students_'], function (){
        Route::get('/', 'AdminStudentController@index')->name('list');
        Route::get('/create', 'AdminStudentController@create')->name('create');
        Route::post('/create', 'AdminStudentController@store')->name('store');
        //Route::get('/{id}/show', 'AdminStudentController@show')->name('show')->where('id', '\d+');
        Route::get('/{id}', 'AdminStudentController@edit')->name('edit');
        Route::put('/{id}', 'AdminStudentController@update')->name('update');
        Route::delete('/{id}', 'AdminStudentController@destroy')->name('destroy');
    });

    // crud учителей
    Route::group(['prefix' => 'teachers', 'as' => 'teachers_'], function (){
        Route::get('/', 'AdminTeacherController@index')->name('list');
        Route::get('/create', 'AdminTeacherController@create')->name('create');
        Route::post('/create', 'AdminTeacherController@store')->name('store');
        Route::get('/{id}', 'AdminTeacherController@edit')->name('edit');
        Route::put('/{id}', 'AdminTeacherController@update')->name('update');
        Route::delete('/{id}', 'AdminTeacherController@destroy')->name('destroy');
    });

    // crud групп
    Route::group(['prefix' => 'groups', 'as' => 'groups_'], function (){
        Route::get('/', 'AdminGroupController@index')->name('list');
        Route::get('/create', 'AdminGroupController@create')->name('create');
        Route::post('/create', 'AdminGroupController@store')->name('store');
        Route::get('/{id}', 'AdminGroupController@edit')->name('edit');
        Route::put('/{id}', 'AdminGroupController@update')->name('update');
        Route::delete('/{id}', 'AdminGroupController@destroy')->name('destroy');
    });

    Route::group(['prefix' => '{roleUser}/{id}/contacts', 'where' => ['roleUser' => 'students|teachers'], 'as' => 'contacts_'], function (){
        Route::get('/', 'AdminContactController@edit')->name('edit');
        Route::put('/', 'AdminContactController@update')->name('update');
    });
    
    // crud предметов
    Route::group(['prefix' => 'subjects', 'as' => 'subjects_'], function (){
        Route::get('/', 'AdminSubjectController@index')->name('list');
        Route::get('/create', 'AdminSubjectController@create')->name('create');
        Route::post('/create', 'AdminSubjectController@store')->name('store');
        Route::get('/{id}', 'AdminSubjectController@edit')->name('edit');
        Route::put('/{id}', 'AdminSubjectController@update')->name('update');
        Route::delete('/{id}', 'AdminSubjectController@destroy')->name('destroy');
    });

    // crud успеваемости
//    Route::group(['prefix' => 'students/{id}/progresses', 'as' => 'progresses_'], function (){
//        Route::get('/', 'AdminProgressController@index')->name('list');
//        Route::get('/create', 'AdminProgressController@create')->name('create');
//        Route::post('/create', 'AdminProgressController@store')->name('store');
//        Route::get('/{id}', 'AdminProgressController@edit')->name('edit');
//        Route::put('/{id}', 'AdminProgressController@update')->name('update');
//        Route::delete('/{id}', 'AdminProgressController@destroy')->name('destroy');
//    });
});

Route::group(['prefix' => 'student', 'middleware' => 'role:' . \App\Models\Role::STUDENT, 'namespace' => 'Student', 'as' => 'students_'], function(){
    Route::get('/', 'StudentController@index')->name('index');
});

Route::group(['prefix' => 'teacher', 'middleware' => 'role:' . \App\Models\Role::TEACHER, 'namespace' => 'Teacher', 'as' => 'teacher_'], function(){
    Route::get('/', 'TeacherController@index')->name('index');
});

Route::post('/login', 'AuthController@login')->name('login');
Route::get('/loginTest', 'AuthController@loginShowTest')->name('loginTest');
Route::post('/loginTest', 'AuthController@loginTest')->name('loginTest');
Route::get('/logout', 'AuthController@logout')->name('logout');

//endregion

//region VUE

Route::group(['as' => 'vue_', '' => 'vue_app', 'middleware' => 'role:' . \App\Models\Role::ADMIN, 'prefix' => 'vue', 'namespace' => 'Admin'], function () {
    Route::get('/', function () {
        return view('vue_app.vue');
    });

    /*Route::group(['prefix' => 'admin'], function ()
    {
        // crud учителей
        Route::group(['prefix' => 'teachers', 'as' => 'teachers_'], function (){
            Route::get('/', 'AdminTeacherController@indexJson');
//            Route::get('/create', 'AdminTeacherController@createJson');
            Route::post('/create', 'AdminTeacherController@storeJson');
            Route::get('/{id}', 'AdminTeacherController@editJson');
            Route::put('/{id}', 'AdminTeacherController@updateJson');
            Route::delete('/{id}', 'AdminTeacherController@destroyJson');
        });

        // crud групп
        Route::group(['prefix' => 'groups'], function (){
            Route::get('/', 'AdminGroupController@indexJson');
            Route::post('/create', 'AdminGroupController@storeJson');
            Route::get('/{id}', 'AdminGroupController@editJson');
            Route::put('/{id}', 'AdminGroupController@updateJson');
            Route::delete('/{id}', 'AdminGroupController@destroyJson');
        });

        // crud предметов
        Route::group(['prefix' => 'subjects', 'as' => 'subjects_'], function (){
            Route::get('/', 'AdminSubjectController@indexJson');
//            Route::get('/create', 'AdminSubjectController@createJson');
            Route::post('/create', 'AdminSubjectController@storeJson');
            Route::get('/{id}', 'AdminSubjectController@editJson');
            Route::put('/{id}', 'AdminSubjectController@updateJson');
            Route::delete('/{id}', 'AdminSubjectController@destroyJson');
        });
    });*/

    // небольшой фоллбек для удобства(не трогать)
    Route::fallback('AdminFallbackController@fallback');
});

//endregion