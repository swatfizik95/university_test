<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api', 'as' => 'vue_', '' => 'vue_app', 'prefix' => 'vue', 'namespace' => 'Admin'], function () {
});

// crud групп
Route::group(['prefix' => 'groups'], function (){
    Route::get('/', 'Api\Admin\ApiAdminGroupController@index');
    Route::post('/create', 'ApiAdminGroupController@store');
    Route::get('/{id}', 'ApiAdminGroupController@edit');
    Route::put('/{id}', 'ApiAdminGroupController@update');
    Route::delete('/{id}', 'ApiAdminGroupController@destroy');
});

Route::group(['namespace' => 'Api'], function (){
    Route::post('/login', 'ApiAuthController@login');

    Route::group(['middleware' => 'auth:api'], function (){
        Route::get('logout', 'ApiAuthController@logout');

        Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function (){

            // crud контактов
            Route::group(['prefix' => '{roleUser}/{id}/contacts', 'where' => ['roleUser' => 'students|teachers']], function (){
                Route::get('/', 'ApiAdminContactController@edit');
                Route::post('/', 'ApiAdminContactController@update');
            });

            // crud групп
            Route::group(['prefix' => 'groups'], function (){
                Route::get('/', 'ApiAdminGroupController@index');
                Route::post('/create', 'ApiAdminGroupController@store');
                Route::get('/{id}', 'ApiAdminGroupController@edit');
                Route::put('/{id}', 'ApiAdminGroupController@update');
                Route::delete('/{id}', 'ApiAdminGroupController@destroy');
            });

            // crud оценок
            Route::group(['prefix' => 'students/{id}/progresses'], function (){});

            // crud студентов
            Route::group(['prefix' => 'students'], function (){
                Route::get('/', 'ApiAdminStudentController@index');
                Route::get('/create', 'ApiAdminStudentController@create');
                Route::post('/create', 'ApiAdminStudentController@store');
                Route::get('/{id}', 'ApiAdminStudentController@edit');
                Route::put('/{id}', 'ApiAdminStudentController@update');
                Route::delete('/{id}', 'ApiAdminStudentController@destroy');
            });

            // crud предметов
            Route::group(['prefix' => 'subjects'], function (){
                Route::get('/', 'ApiAdminSubjectController@index');
                Route::get('/create', 'ApiAdminSubjectController@create');
                Route::post('/create', 'ApiAdminSubjectController@store');
                Route::get('/{id}', 'ApiAdminSubjectController@edit');
                Route::put('/{id}', 'ApiAdminSubjectController@update');
                Route::delete('/{id}', 'ApiAdminSubjectController@destroy');
            });

            // crud учителей
            Route::group(['prefix' => 'teachers'], function (){
                Route::get('/', 'ApiAdminTeacherController@index');
                Route::post('/create', 'ApiAdminTeacherController@store');
                Route::get('/{id}', 'ApiAdminTeacherController@edit');
                Route::put('/{id}', 'ApiAdminTeacherController@update');
                Route::delete('/{id}', 'ApiAdminTeacherController@destroy');
            });
        });

    });
});
